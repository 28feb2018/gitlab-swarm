Stack.yaml and configs for deploying Gitlab via Docker Swarm

Deploy with:
```sh
sudo docker stack deploy -c stack.yaml gitlab
```